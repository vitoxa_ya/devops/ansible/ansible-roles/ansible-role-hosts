Role Docker
=========

Роль предназначена для создания мини dns сервера, записывает все сервера и также приложения в контейнерахв /etc/hosts

Role Variables
--------------

#### Переменные пользователя:
  
      hosts_add: true - false отключает запись Хостов
      docker_to_hosts: false - true включает запись docker хостов в /etc/hosts
  ___  


Example Playbook
----------------


    - hosts: servers
      roles:
        - role: 
          - username.rolename
          vars:
            hosts_add: false
            docker_to_hosts: true

License
-------

MIT

Author Information
------------------

vitoxaya
